<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
<link href="http://dialuscss.dialus.com/dialus.css" rel="stylesheet" type="text/css" media="all"/>
<script type="text/javascript" src="js/salesTrackingValid.js"></script> 
<script>
	jQuery(function(){
		 
		
			
		
		 $('#edittelecaller_state').autocomplete('CityautoCompleteAction.action?variable=state');
			$('#edittelecaller_city').autocomplete('CityautoCompleteAction.action',
					
					{
				  extraParams:
					
				        {
					  businessState: function () {
				                return $("#edittelecaller_state").val();
				            },
				            
				            
				            variable: function () {
				                return "city";
				            }
				
				        }
					}
			
			
			);
			
			/*area starts here  */
			
			
			
			/*Start of area auto complete  */
		$('#edittelecaller_area').autocomplete('CityautoCompleteAction.action',
					
					{
				  extraParams:
					
				        {
					  businesCity: function () {
				                return $("#edittelecaller_city").val();
				            },
				            
				            
				            variable: function () {
				                return "area";
				            }

				        }
					}
			
			
			);
	});
</script>
</head>
<body>

						      	<s:form action="editUsersAction" theme="simple">
								    	<s:hidden name="objUsersOperationsDTO.usertype" value="telecaller"></s:hidden>
								    	<s:hidden name="objUsersOperationsDTO.userId" value="%{objUsersOperationsDTO.userId}"></s:hidden>
								        <div class="main_div_middle">
								         <div class="main_div_middle_textfields_main" style="text-align:center;background: #008ABB;width: 100%;color:#fff;padding: 8px 0px;margin-top:-10px;border-top-left-radius:10px;border-top-right-radius:10px;font-family: georgia;">
											            TeleSales Persons   Details
											        </div>
									        <div class="appointmentform_left1" style="margin-left:200px;">
                                	<div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                      <b style="color:red">&lowast;</b>TeleSales Person Name :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objUsersOperationsDTO.userName" value="%{objUsersOperationsDTO.userName}" maxlength="45" id="edittelecaller_name"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <%-- <%-- <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       Telecaller State :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objUsersOperationsDTO.userState" value="%{objUsersOperationsDTO.userState}"  maxlength="45"    id="edittelecaller_state"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div> --%>
                                   <%--  <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                      Telecaller  City :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objUsersOperationsDTO.userCity" value="%{objUsersOperationsDTO.userCity}"  maxlength="45"   id="edittelecaller_city"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                     <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       Telecaller Area :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objUsersOperationsDTO.userArea" value="%{objUsersOperationsDTO.userArea}"  maxlength="45"   id="edittelecaller_area"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div> --%> 
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                      <b style="color:red">&lowast;</b>TeleSales Person  Mobile :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objUsersOperationsDTO.userMobile" value="%{objUsersOperationsDTO.userMobile}" maxlength="10"    id="edittelecaller_mobile"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                   <%--  <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        Telecaller Mobile2 :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objUsersOperationsDTO.userMobile1" value="%{objUsersOperationsDTO.userMobile1}"  maxlength="10"     id="edittelecaller_mobile1"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div> --%>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                     TeleSales Person Mail Id :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objUsersOperationsDTO.userMailId" value="%{objUsersOperationsDTO.userMailId}"  maxlength="70"    id="edittelecaller_mailid"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                     <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                     <b style="color:red">&lowast;</b>Password :
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objUsersOperationsDTO.userPassword"  value="%{objUsersOperationsDTO.userPassword}"  maxlength="10"    id="edittelecaller_password"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                     <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                     <b style="color:red">&lowast;</b>Confirm Password:
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield   value="%{objUsersOperationsDTO.userPassword}"  maxlength="10"    id="edittelecaller_confirmpassword"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                   
                                      
                                      <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                    
                                        </div>
                                        
                                        <div class="appointmentform_left_right" style="float: right;">
                                      <s:submit value="Update User Details" id="edittelecaller_but"></s:submit>
                                        </div>
                                    </div>
                                    </div>
									            
									            
									          
									        </div>
								     
								     </s:form>
							
				    
</body>
</html>