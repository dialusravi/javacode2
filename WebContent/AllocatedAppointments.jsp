<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
        <%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="css/css.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="css/jquery-ui123.css" />	 
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script src="js/jquery-min.js"></script>

</head>
<body>
<p align="center" style="font-size: 22px;color: red;">Allocated Appointments</p>
<div class="userappointments_line"></div>

<div class="userappointments_line"></div>
<div class="userappointments_line" style="margin-top:10px;"></div>

<div class="userappointments">
	
    <div class="user_appointmentplace" style="width:120px">
    Business Name
    </div>
    
    <div class="user_appointmentfixedby" style="width:135px;">
   Appointment Date 
    </div>
      <div class="user_appointmentfixedby" style="width:135px;">
     SalesPersonName
    </div>
     <div class="user_appointmentfixedby" style="width:135px;">
     Business City
    </div>
    <div class="user_appointmentfixedby" style="width:135px;">
     Business Area
    </div>
 
</div>

<div class="userappointments_line"></div>
<s:if test="%{objNamesList.size!=0}">
<s:iterator value="objNamesList" id="a">
<div class="userappointments_arabicner1">


  <div class="userbusinessname" style="width:120px">
   <s:if test="%{#a.businessName!=null}">
    <s:property value="#a.businessName"/>
</s:if>
<s:else>
-----------------
</s:else>
  </div>
  
  
  
    <div class="user_appointmentplace" style="width:135px;">
    
    <s:if test="%{#a.date!=null}">
    <s:property value="#a.date"/>
    </s:if>
    <s:else>
-----------------
</s:else>
    </div>
    
    <div class="user_appointmenttime" style="width:135px">
    <s:if test="%{#a.name!=null}">
    <s:property value="#a.name"/>
    </s:if>
     <s:else>
-----------------
</s:else>
    </div>
    
    
    <div class="user_appointmenttime" style="width:135px">
    <s:if test="%{#a.businessCity!=null}">
    <s:property value="#a.businessCity"/>
    </s:if>
     <s:else>
-----------------
</s:else>
    
    </div>
    <div class="user_appointmenttime" style="width:135px">
    <s:if test="%{#a.businessArea!=null}">
    <s:property value="#a.businessArea"/>
    </s:if>
    <s:else>
-----------------
</s:else>
    </div>
    
</div>

</s:iterator>
</s:if>
<s:else>
<div class="userappointments_arabicner1" style="color:red;font-family: georgia;font-weight: bold">Sorry No Records Are  Available....</div>
</s:else>

<div class="newmyprofile_userfrofile_businessname" style="width:120px;margin-left: 270px;color: red;font-size: 21px;" >
        Arabic Data 
        </div>


<div class="userappointments">
	
    <div class="user_appointmentplace" style="width:120px">
    Business Name
    </div>
    
    <div class="user_appointmentfixedby" style="width:135px;">
   Appointment Date 
    </div>
      <div class="user_appointmentfixedby" style="width:135px;">
     SalesPersonName
    </div>
     <div class="user_appointmentfixedby" style="width:135px;">
     Business City
    </div>
    <div class="user_appointmentfixedby" style="width:135px;">
     Business Area
    </div>
 
</div>

<div class="userappointments_line"></div>
<s:if test="%{objNamesList1.size!=0}">
<s:iterator value="objNamesList1" id="a">
<div class="userappointments_arabicner1">


  <div class="userbusinessname" style="width:120px">
   <s:if test="%{#a.arabicBusinessName!=null}">
    <s:property value="#a.arabicBusinessName"/>
</s:if>
<s:else>
-----------------
</s:else>
  </div>
  
  
  
    <div class="user_appointmentplace" style="width:135px;">
    
    <s:if test="%{#a.date!=null}">
    <s:property value="#a.date"/>
    </s:if>
    <s:else>
-----------------
</s:else>
    </div>
    
    <div class="user_appointmenttime" style="width:135px">
    <s:if test="%{#a.name!=null}">
    <s:property value="#a.name"/>
    </s:if>
     <s:else>
-----------------
</s:else>
    </div>
    
    
    <div class="user_appointmenttime" style="width:135px">
    <s:if test="%{#a.arabicBusinessCity!=null}">
    <s:property value="#a.arabicBusinessCity"/>
    </s:if>
     <s:else>
-----------------
</s:else>
    
    </div>
    <div class="user_appointmenttime" style="width:135px">
    <s:if test="%{#a.arabicBusinessArea!=null}">
    <s:property value="#a.arabicBusinessArea"/>
    </s:if>
    <s:else>
-----------------
</s:else>
    </div>
    
</div>

</s:iterator>
</s:if>
<s:else>
<div class="userappointments_arabicner1" style="color:red;font-family: georgia;font-weight: bold">Sorry No Records Are  Available....</div>
</s:else>
</body>
</html>