<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/css.css" rel="stylesheet" type="text/css" media="all" />
<script src="js/jquery-min.js"></script>
<link rel="stylesheet" href="css/jquery-ui123.css" />	 
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/themes/redmond/jquery-ui.css" />

<!-- Plugin files below -->
<link rel="stylesheet" type="text/css" href="css/jquery.ptTimeSelect.css" />
<script type="text/javascript" src="js/jquery.ptTimeSelect.js"></script>
<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
<link href="http://dialuscss.dialus.com/dialus.css" rel="stylesheet" type="text/css" media="all"/>
<script type="text/javascript" src="js/salesTrackingValid.js"></script>
<script>
	
	jQuery(function(){
		 		
		    $(".arabic_keyword_id").autocomplete("ArabicAutoCompleteAction.action");
		    $('#businessState').autocomplete('CityautoCompleteAction.action?variable=state');
			$('#businessCity').autocomplete('CityautoCompleteAction.action',
					
					{
				  extraParams:
					
				        {
					  businessState: function () {
				                return $("#businessState").val();
				            },
				            
				            
				            variable: function () {
				                return "city";
				            }
				
				        }
					}
			
			
			);
			
			/*area starts here  */
			
			
			
			/*Start of area auto complete  */
		$('#businessArea').autocomplete('CityautoCompleteAction.action',
					
					{
				  extraParams:
					
				        {
					  businesCity: function () {
				                return $("#businessCity").val();
				            },
				            
				            
				            variable: function () {
				                return "area";
				            }

				        }
					}
			
			
			);
			/* end of area autocomplete */
			
			
			/* START of served area autocomplete   */
			
			
			
		
			/* End of SERVED AREA AUTOCOMPLETE */
			
			
		$('#appointmentState').autocomplete('CityautoCompleteAction.action?variable=state');
		$('#appointmentCity').autocomplete('CityautoCompleteAction.action',
				
				{
			  extraParams:
				
			        {
				  businessState: function () {
			                return $("#appointmentState").val();
			            },
			            
			            
			            variable: function () {
			                return "city";
			            }
			
			        }
				}
		
		
		);
		
		/*area starts here  */
		
		
		
		/*Start of area auto complete  */
	$('#appointmentPlace').autocomplete('CityautoCompleteAction.action',
				
				{
			  extraParams:
				
			        {
				  businesCity: function () {
			                return $("#appointmentCity").val();
			            },
			            
			            
			            variable: function () {
			                return "area";
			            }

			        }
				}
		
		
		);
		/* end of area autocomplete */
		
		
		/* START of served area autocomplete   */
		
			
		});
	</script>
<script type="text/javascript">
        $(document).ready(function()
        {
        	  $('#appointmentTime').attr("placeholder", "Click This For Time");
            // find the input fields and apply the time select to them.
            $('#appointmentTime').ptTimeSelect();
            $("#myOwnCategory").hide();
          
            
            $('.datepicker').datepicker({minDate: 0});
        });
    </script>
      <script>
  $(function()
  {
    $( ".datepicker" ).datepicker();
    $(".datepicker").datepicker("setDate", new Date);
    
  });
  </script>
<script type="text/javascript">
function date_time(id)
{
        date = new Date;
        year = date.getFullYear();
        month = date.getMonth();
        months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'Jully', 'August', 'September', 'October', 'November', 'December');
        d = date.getDate();
        day = date.getDay();
        days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
        h = date.getHours();
        if(h<10)
        {
                h = "0"+h;
        }
        m = date.getMinutes();
        if(m<10)
        {
                m = "0"+m;
        }
        s = date.getSeconds();
        if(s<10)
        {
                s = "0"+s;
        }
        result =  ''+d+' '  +months[month]+'  '+year+' -  '+h+':'+m+':'+s;
        document.getElementById(id).innerHTML = result;
        setTimeout('date_time("'+id+'");','1000');
        return true;
}
</script>
<script type="text/javascript">
        $(document).ready(function(){
        	$("#myOwnCategory").hide();
            // find the input fields and apply the time select to them.
            $('#appointmentTime').ptTimeSelect();
            $("#callbackdate_div_date").hide();
            
            $("#check_callback").click(function()
            {
            	if($('#check_callback').is(':checked'))
            	{
            		//attr('checked')
	                $("#callback_div").hide();
	                $("#callbackdate_div_date").show();
	                document.getElementById('update_appointment').value='Save As Call Back';
	               // $("#fix_appointment_but").html("Save As Call Back");
	                $('#check_save').removeAttr('checked');
	                $('#check_fix').removeAttr('checked');
	                $('#appointmentDate').val(" ");
	                
	                
	                
            	}
            	else
           		{
            		$("#callback_div").show();
 	                $("#callbackdate_div_date").hide();
 	               document.getElementById('update_appointment').value='Fix Appointment';
 	               	//$("#fix_appointment_but").html("Fix Appointment");
 	               $('#check_save').removeAttr('checked');
 	              $('#check_fix').removeAttr('checked');
           		}
            	
                
            });
            
            $("#check_save").click(function()
                    {
                    	if($('#check_save').is(':checked'))
                    	{
                    		//attr('checked')
        	                $("#callback_div").hide();
        	                $("#callbackdate_div_date").hide();
        	                document.getElementById('update_appointment').value='Add (or) Save Data';
        	              //  $("#fix_appointment_but").html("Add(or)Save Data");
        	                $('#check_callback').removeAttr('checked');
        	                $('#check_fix').removeAttr('checked');
        	                $('#appointmentDate').val("");
        	                $('#callBackDate').val("");
        	                
                    	}
                    	else
                   		{
                    		 $("#callback_div").show();
         	                $("#callbackdate_div_date").hide();
         	               document.getElementById('update_appointment').value='Fix Appointment';
         	              // $("#fix_appointment_but").html("Fix Appointment");
         	              $('#check_callback').removeAttr('checked');
         	             $('#check_fix').removeAttr('checked');
         	             // $('#').prop('checked', false); 
                   		}
                        
                    });
            
            
            $("#check_fix").click(function()
                    {
                    	if($('#check_fix').is(':checked'))
                    	{
                    		//attr('checked')
        	                $("#callback_div").show();
        	                $("#callbackdate_div_date").hide();
        	                document.getElementById('update_appointment').value='Fix Appointment';
        	              //  $("#fix_appointment_but").html("Add(or)Save Data");
        	                $('#check_callback').removeAttr('checked');
        	                $('#check_save').removeAttr('checked');
        	                $('#callBackDate').val("");
                    	}
                    	else
                   		{
                    		 $("#callback_div").hide();
         	                $("#callbackdate_div_date").hide();
         	               document.getElementById('update_appointment').value='Fix Appointment';
         	              // $("#fix_appointment_but").html("Fix Appointment");
         	              $('#check_callback').removeAttr('checked');
         	             $('#check_save').removeAttr('checked');
         	             // $('#').prop('checked', false); 
                   		}
                        
                    });
            
            
            
            $('.datepicker').datepicker({minDate: 0});
        });
    </script>
    <s:if test="%{objCheckAppointmentDTO.callBackDate!='1900-01-01' && objCheckAppointmentDTO.appointmentDate=='1900-01-01'}">
	    <script>
	    $(document).ready(function()
	    {
	    	//alert("3");
	    	$("#callback_div").hide();
	    	$("#callbackdate_div_date").show();
	    	$("#appointmentDate").val("");
	    	$("#appointmentTime").val("");
	    	
	    	
	    	
		    document.getElementById('update_appointment').value='Save As Call Back';
		   // $("#fix_appointment_but").html("Save As Call Back");
		    $("#check_callback").attr("checked", true);
		  // $('#check_callback').addAttr('checked');
		    $('#check_save').removeAttr('checked');
		    $('#check_fix').removeAttr('checked');
	    });
	    
	    </script>
	    
    
    </s:if> 
    <s:if test="%{objCheckAppointmentDTO.callBackDate=='1900-01-01' && objCheckAppointmentDTO.appointmentDate!='1900-01-01'}">
	    <script>
	    $(document).ready(function()
	    {
	    	//alert("2");
	    	  $("#callback_div").show();
	    	  $("#callBackDate").val("");
              $("#callbackdate_div_date").hide();
              document.getElementById('update_appointment').value='Fix Appointment';
            //  $("#fix_appointment_but").html("Add(or)Save Data");
            $("#check_fix").attr("checked", true);
            // $('#check_fix').addAttr('checked');
              $('#check_callback').removeAttr('checked');
              $('#check_save').removeAttr('checked');
              
	    });
	    
	    </script>
    </s:if>
	<s:if test="%{objCheckAppointmentDTO.callBackDate=='1900-01-01' && objCheckAppointmentDTO.appointmentDate=='1900-01-01'}">
	    <script>
	    $(document).ready(function()
	    {
	    	//alert("1");
	    	  $("#callback_div").hide();
	    	  $("#callBackDate").val("");
	    	  $("#appointmentDate").val("");
	    	  $("#appointmentTime").val("");
              $("#callbackdate_div_date").hide();
              document.getElementById('update_appointment').value='Fix Appointment';
            //  $("#fix_appointment_but").html("Add(or)Save Data");
             	$("#check_save").attr("checked", true);
            	//$('#check_save').addAttr('checked');
              $('#check_callback').removeAttr('checked');
              $('#check_fix').removeAttr('checked');
              
	    });
	    
	</script>
	    
    
    </s:if> 
   
</head>

<body>
<div class="top_bg">
	<div class="topbg_arabicner">
   	  <div class="topbg_arabicner_left">
        	<div class="topbg_arabicner_left_top">
            SAUDI SALES TRACKING
            </div>
            <div class="topbg_arabicner_left_bottom">
             <span id="date_time"></span>
            <script type="text/javascript">window.onload = date_time('date_time');</script>
          </div>
      </div>
      <div class="topbg_arabicner_right">
        	<div class="topbg_arabicner_right_left">
       	  <img src="images/dialus_small.png" width="28" height="27" /> </div>
            <div class="topbg_arabicner_right_right">
            9666 888 888
            </div>
      </div>
    </div>
</div>
<div style="clear:both"></div>
<div class="wrapper">
<div class="logopart">
	<div class="logopart_left">
    <img src="images/logo.jpg" width="225" height="90" /> 
    </div>
    <div class="logopart_right">
    
    	<!-- <ul>
        
        <li>About Us</li>
         <li>|</li>
        <li>Services</li>
         <li>|</li>
        <li>Gallery</li>
          <li>|</li>
        <li>Products</li>
          <li>|</li>
        <li>Contact Us</li>
        </ul> -->
    </div>
</div>
<s:if test="%{#request.success_msg!=null && #request.success_msg!=''}">
		<div style="text-align: center;width:980px;" id="success_msg"> <font style="color: green;font-family: georgia;font-weight: bold;"><s:property value="%{#request.success_msg}"/></font></div>
	</s:if>
<div class="contentpart">
	<div class="content_left">
    	
        <div id="login_links">
	        <div class="content_left_headding">
		  <div class="content_left_text">
            Menu
          </div>
      </div>
	       <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a href="telecallerHome.jsp" style="cursor: pointer;" id="home_link" >Home</a>
	        	</div>
	       </div>
	         
	       </div>
	       
	      
	       <div class="smallheadding_conleft_arabicner">
	       		<div class="smallheadding_conleft_arabicner_left">
	         		<img src="images/arrow.jpg" width="5" height="8" /> 
	        	</div>
	        
	        	<div class="smallheadding_conleft_arabicner_right">
	        		 <a href="LogOut.action" style="cursor: pointer;text-decoration: none;color:#f30909;" id="logout_link">Log Out</a>
	        	</div>
	       </div>
      </div>
      
   
    
	   	  	   
    <div class="content_right">
    <s:form action="storeAppointmentDetails2" theme="simple">
					    	<s:hidden name="usertype" value="user"></s:hidden>
					    	<s:hidden name="objCheckAppointmentDTO.appointmentId" value="%{objCheckAppointmentDTO.appointmentId}"></s:hidden>
					        <div class="main_div_middle">
					         	<div class="main_div_middle_textfields_main" style="text-align:center;background: #008ABB;width: 100%;color:#fff;padding: 8px 0px;margin-top:-10px;border-top-left-radius:10px;border-top-right-radius:10px;">
								           Appointments
								</div>
                                
                                <div class="appointmentform">
                                <div class="appointmentform_left1">
                                	<div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>الاسم التجاري
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.arabicBusinessName"   maxlength="130" id="businessName" value="%{objCheckAppointmentDTO.arabicBusinessName}" cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <s:hidden name="objCheckAppointmentDTO.businessCategory_old"  value="%{objCheckAppointmentDTO.businessCategory}" ></s:hidden>
                                    <s:hidden name="objCheckAppointmentDTO.businesssubcategory_old"  value="%{objCheckAppointmentDTO.businesssubcategory}" ></s:hidden>
                                    <s:hidden name="objCheckAppointmentDTO.businessOtherCategory_old" value="%{objCheckAppointmentDTO.businessOtherCategory}" ></s:hidden>
                                    
                               
                                     <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>دولة العمل
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.arabicBusinessState"     maxlength="45"  value="%{objCheckAppointmentDTO.arabicBusinessState}"  id="businessState"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    
                                     <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>مدينة العمل
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.arabicBusinessCity"    maxlength="45"  value="%{objCheckAppointmentDTO.arabicBusinessCity}"  id="businessCity"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                        
                                    </div>
                                    
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>منطقة العمل
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.arabicBusinessArea"    maxlength="45"  value="%{objCheckAppointmentDTO.arabicBusinessArea}"  id="businessArea"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    
                                  <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>1 الكلمة
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.arabickeyword1"  cssClass="arabic_keyword_id"  maxlength="100"  value="%{objCheckAppointmentDTO.arabickeyword1}"  id="keyword1"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    
                                   <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>2 الكلمة
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.arabickeyword2" cssClass="arabic_keyword_id"    maxlength="100"  value="%{objCheckAppointmentDTO.arabickeyword2}"  id="keyword2"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    
                                   <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red"></b>3 الكلمة
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.arabickeyword3" cssClass="arabic_keyword_id"    maxlength="100"  value="%{objCheckAppointmentDTO.arabickeyword3}"  id="keyword3"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    
                                   <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red"></b>4 الكلمة
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.arabickeyword4"  cssClass="arabic_keyword_id"   maxlength="100"  value="%{objCheckAppointmentDTO.arabickeyword4}"  id="keyword4"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                   </div>
                                    
                                   <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red"></b>5 الكلمة
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.arabickeyword5"  cssClass="arabic_keyword_id"   maxlength="100"  value="%{objCheckAppointmentDTO.arabickeyword5}"  id="keyword5"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        <b style="color:red">&lowast;</b>رقم جوال العمل 
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objCheckAppointmentDTO.businessMobile"  readonly="true"   maxlength="10"   value="%{objCheckAppointmentDTO.businessMobile}"   id="businessMobile"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                      رقم هاتف العمل
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objCheckAppointmentDTO.businessPhone"   maxlength="10"   value="%{objCheckAppointmentDTO.businessPhone}"  id="businessPhone"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       رقم هاتف العمل 1
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textfield  name="objCheckAppointmentDTO.businessPhone1"    maxlength="10"  value="%{objCheckAppointmentDTO.businessPhone1}"  id="businessPhone1"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    
                                   <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       البريد الالكتروني
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.businessMail"  maxlength="45"   value="%{objCheckAppointmentDTO.businessMail}"  id="businessMail"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    
                                     
                                    </div>
                                  <div class="appointmentform_right">
                                  <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       <b style="color:red">&lowast;</b>رقم الهاتف
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.contactNumber"   maxlength="10"  value="%{objCheckAppointmentDTO.contactNumber}"  id="contactNumber"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        <b style="color:red">&lowast;</b>رقم الشخص
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                       <s:textfield  name="objCheckAppointmentDTO.contactPerson"   maxlength="43"  value="%{objCheckAppointmentDTO.contactPerson}"  id="contactPerson"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
                                        </div>
                                    </div>
                            
                                     <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left" style="width:230px;text-align: right;">
                                       			<b style="color:red;white-space: nowrap;">Check Here if Call Back</b>
                                        </div>
                                        
                                        <div class="appointmentform_left_right" style="width:20px;">
                                        <input type="checkbox" id="check_callback"/>
                                        </div>
                                    </div>
                                    
                                     <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left" style="width:230px;text-align: right;">
                                       			<b style="color:red;white-space: nowrap;">Check Here if Save Data</b>
                                        </div>
                                        
                                        <div class="appointmentform_left_right" style="width:20px;">
                                        <input type="checkbox" id="check_save"/>
                                        </div>
                                    </div>
                                    
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left" style="width:230px;text-align: right;">
                                       			<b style="color:red;white-space: nowrap;">Check Here if Fix Appointment</b>
                                        </div>
                                        
                                        <div class="appointmentform_left_right" style="width:20px;">
                                        <input type="checkbox" id="check_fix" />
                                        </div>
                                    </div>
                                   
	                                    <div id="callbackdate_div_date" style="display:none;">
		                                    <div class="appointmentform_left">
			                                    	<div class="appointmentform_left_left">
			                                       <b style="color:red">&lowast;</b>تاريخ الرد
			                                        </div>
			                                        
			                                        <div class="appointmentform_left_right">
			                                        <s:textfield  name="objCheckAppointmentDTO.callBackDate" cssClass="datepicker" readonly="true" value="%{objCheckAppointmentDTO.callBackDate}"  id="callBackDate"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
			                                        </div>
			                                </div>
	                                    </div>
                                 
                                    
                                    <div id="callback_div" style="display: none;">
	                                     <div class="appointmentform_left">
	                                    	<div class="appointmentform_left_left">
	                                       <b style="color:red">&lowast;</b>تاريخ المقابلة
	                                        </div>
	                                        
	                                        <div class="appointmentform_left_right">
	                                        <s:textfield  name="objCheckAppointmentDTO.appointmentDate" cssClass="datepicker" readonly="true" value="%{objCheckAppointmentDTO.appointmentDate}"  id="appointmentDate"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
	                                        </div>
	                                    </div>
	                                     <div class="appointmentform_left">
	                                    	<div class="appointmentform_left_left">
	                                       <b style="color:red">&lowast;</b>وقت المقابلة
	                                        </div>
	                                        
	                                        <div class="appointmentform_left_right">
	                                        <s:textfield  name="objCheckAppointmentDTO.appointmentTime" readonly="true"  value="%{objCheckAppointmentDTO.appointmentTime}"   id="appointmentTime"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textfield>
	                                        </div>
	                                    </div>
                                    </div>
                                    
                                    
                                
                                   
                                    <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                        تعليقات
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        <s:textarea  name="objCheckAppointmentDTO.comments" value="%{objCheckAppointmentDTO.comments}"    id="comments"  cssStyle="width:150px; border:1px solid #008abb; padding:5px 0" ></s:textarea>
                                        </div>
                                    </div>
                                    
                                     <div class="appointmentform_left">
                                    	<div class="appointmentform_left_left">
                                       حالة الاتصال
                                        </div>
                                        
                                        <div class="appointmentform_left_right">
                                        Answered<input type="radio" checked="checked"  name="objCheckAppointmentDTO.callStatus" value="0"/>
                                        
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                 
                                <div class="appointmentform" style="margin-top:15px;">
                               
                               
                                 	<s:submit  id="update_appointment"  value="Update Appointment Details" cssStyle="color:#fff; font-weight:bold; background-color:#008abb; text-align:center; padding:5px 0 5px 0; width:328px; border:none; margin:0 0 0 250px;" />
                               
                                </div>
                                
                                
                                
                                
					        </div>
					     
					     </s:form>
	
</div>
 </div>
</div>
<div class="fotter">
	<div class="fotter_arabicner">
     All Rights Reserved - Copyright Â© Dialus.com 2010-2013
    </div>
</div>
</body>
</html>
