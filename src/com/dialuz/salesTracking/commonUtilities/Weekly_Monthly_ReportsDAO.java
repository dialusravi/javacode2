package com.dialuz.salesTracking.commonUtilities;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import com.dialuz.salesTracking.DB.DBConfig;
import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

public class Weekly_Monthly_ReportsDAO 
{

	//private String path="/var/opt/tomcat8888/webapps/ROOT/images/reports/monthlyFreelisting.xls";
	

	//private String path="/home/raju/Desktop/raju/SalesTracking/SalesTrackingProject/WebContent/EOD";
	
	private Connection conn;
	private PreparedStatement ps;
	private ResultSet rs = null;
	private PreparedStatement ps2;
	private ResultSet rs2= null;
	private File file;
	public String getMonthlyReports(String path) throws Exception 
	{
		ArrayList<String> count=new ArrayList<String>();
		List details = null;
		String query;
		String query1;
		int numberOfColumns = 0;
		ResultSetMetaData rsmd=null;
		Label lbl;
		Iterator itr;
		try {

			conn =DBConfig.connect();
			query="";
			
			
			query1="";
			
			try{
			ps = conn.prepareStatement(query);
			rs = ps.executeQuery();
			rsmd = rs.getMetaData();
			numberOfColumns = rsmd.getColumnCount();
			details = new ArrayList();
			// Read and store data in list variable.
			while (rs.next()) {
				for (int n = 1; n <= numberOfColumns; n++) {
					details.add(rs.getString(n));
				}
			}
			}catch (Exception e) {
				// TODO: handle exception
			}
			try{
				ps2 = conn.prepareStatement(query1);
				rs2 = ps2.executeQuery();
				while(rs2.next())
				{
					count.add(rs2.getString("records"));
				}
			}catch (Exception e) {
				// TODO: handle exception
			}
			// Excel file properties
			file=new File(path);
			if(!file.exists()){
				file.createNewFile();
			}
			WorkbookSettings wbSettings = new WorkbookSettings();

			wbSettings.setLocale(new Locale("en", "EN"));
			WritableWorkbook w = Workbook.createWorkbook(file, wbSettings);
			w.createSheet("Week End Report", 0);
			WritableSheet s = w.getSheet(0);
			
			WritableFont wf = new WritableFont(WritableFont.ARIAL, 10,
					WritableFont.NO_BOLD);
			WritableCellFormat cf = new WritableCellFormat(wf);
			cf.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);
			cf.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THICK, jxl.format.Colour.BLUE2);
			cf.setWrap(true);

			itr = details.iterator();
			// Write column header
			CellView cv = s.getColumnView(0);
			cv.setAutosize(true);
			cv.setSize(1000);
			s.setColumnView(0, cv);
			s.setColumnView(1, cv);
			
			int col = 0;
			for (int j = 1; j <= numberOfColumns; j++) 
			{
				lbl = new Label(col, 0, rsmd.getColumnName(j), cf);
				
				s.addCell(lbl);
				col++;
			}
			// Write content
			int row = 1;
			while (itr.hasNext()) {
				for (int column = 0; column < numberOfColumns; column++) {
					lbl = new Label(column, row, (String) itr.next(), cf);
					s.addCell(lbl);
				}
				row++;
			}
			
			
			s.addRowPageBreak(row);
			s.addRowPageBreak(row++);
			
			lbl=new Label(0,row++,"No.of.Records Remaining In This Month", cf);
			s.addCell(lbl);
			lbl=new Label(1,row-1,count.get(0), cf);
			s.addCell(lbl);
			lbl=new Label(0,row++,"No.of.Records Pending In This Month", cf);
			s.addCell(lbl);
			lbl=new Label(1,row-1,count.get(1), cf);
			s.addCell(lbl);
			lbl=new Label(0,row++,"No.of.Records Commited In This Month", cf);
			s.addCell(lbl);
			lbl=new Label(1,row-1,count.get(2), cf);
			s.addCell(lbl);
			lbl=new Label(0,row++,"No.of.Records Rejected In This Month", cf);
			s.addCell(lbl);
			lbl=new Label(1,row-1,count.get(3), cf);
			s.addCell(lbl);
			
			s.addRowPageBreak(row);
			s.addRowPageBreak(row++);
			lbl=new Label(0,row++,"Total No.of.All Records  In This Month", cf);
			s.addCell(lbl);
			lbl=new Label(1,row-1,count.get(4), cf);
			s.addCell(lbl);
			
			
			s.addRowPageBreak(row);
			s.addRowPageBreak(row++);
			
			lbl=new Label(0,row++,"Total No.of.Records Remaining", cf);
			s.addCell(lbl);
			lbl=new Label(1,row-1,count.get(5), cf);
			s.addCell(lbl);
			lbl=new Label(0,row++,"Total No.of.Records Pending", cf);
			s.addCell(lbl);
			lbl=new Label(1,row-1,count.get(6), cf);
			s.addCell(lbl);
			lbl=new Label(0,row++,"Total No.of.Records Commited", cf);
			s.addCell(lbl);
			lbl=new Label(1,row-1,count.get(7), cf);
			s.addCell(lbl);
			lbl=new Label(0,row++,"Total No.of.Records Rejected", cf);
			s.addCell(lbl);
			lbl=new Label(1,row-1,count.get(8), cf);
			s.addCell(lbl);
			
			s.addRowPageBreak(row);
			
			lbl=new Label(0,row++,"Total No.of. All Records", cf);
			s.addCell(lbl);
			lbl=new Label(1,row-1,count.get(9), cf);
			s.addCell(lbl);
			w.write();
			w.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			DBConfig.disconnect(conn, ps, rs);
			
		}
		return path;
		
		
		
	}

}
