package com.dialuz.salesTracking.DTO;

import java.io.Serializable;

public class LoginDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1484512186596665074L;
	private String username;
	private String password;
	private String usertype;
	private String dbUserType;
	private String userMobile;
	private boolean loginStatus;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public String getDbUserType() {
		return dbUserType;
	}
	public void setDbUserType(String dbUserType) {
		this.dbUserType = dbUserType;
	}
	public boolean isLoginStatus() {
		return loginStatus;
	}
	public void setLoginStatus(boolean loginStatus) {
		this.loginStatus = loginStatus;
	}
	public String getUserMobile() {
		return userMobile;
	}
	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

}
