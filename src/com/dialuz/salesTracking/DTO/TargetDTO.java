package com.dialuz.salesTracking.DTO;


public class TargetDTO
{

    public TargetDTO()
    {
    }

    public int getToday_target()
    {
        return today_target;
    }

    public void setToday_target(int today_target)
    {
        this.today_target = today_target;
    }

    public int getWeek_target()
    {
        return week_target;
    }

    public void setWeek_target(int week_target)
    {
        this.week_target = week_target;
    }

    public int getMonth_target()
    {
        return month_target;
    }

    public void setMonth_target(int month_target)
    {
        this.month_target = month_target;
    }

    public int getDay_count()
    {
        return day_count;
    }

    public void setDay_count(int day_count)
    {
        this.day_count = day_count;
    }

    public int getWeek_count()
    {
        return week_count;
    }

    public void setWeek_count(int week_count)
    {
        this.week_count = week_count;
    }

    public int getMonth_count()
    {
        return month_count;
    }

    public void setMonth_count(int month_count)
    {
        this.month_count = month_count;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public int getTargets()
    {
        return targets;
    }

    public void setTargets(int targets)
    {
        this.targets = targets;
    }

    public String getDate()
    {
        return date;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    private int day_count;
    private int week_count;
    private int month_count;
    private int today_target;
    private int week_target;
    private int month_target;
    private String username;
    private int targets;
    private String date;
}