package com.dialuz.salesTracking.action;

import com.dialuz.salesTracking.DAO.AppointmentDAO;
import com.dialuz.salesTracking.DTO.AppointmentDTO;
import com.opensymphony.xwork2.ActionSupport;

public class GetContactDetailsAction extends ActionSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6532866589680970450L;
	private String salesPersonName;
	private AppointmentDTO objCheckAppointmentDTO=new AppointmentDTO();
	
	public String getSalesPersonName() {
		return salesPersonName;
	}
	public void setSalesPersonName(String salesPersonName) {
		this.salesPersonName = salesPersonName;
	}
	public AppointmentDTO getObjCheckAppointmentDTO() {
		return objCheckAppointmentDTO;
	}
	public void setObjCheckAppointmentDTO(AppointmentDTO objCheckAppointmentDTO) {
		this.objCheckAppointmentDTO = objCheckAppointmentDTO;
	}
	
	
	public String execute()
	{
		AppointmentDAO objAppointmentDAO=new AppointmentDAO();
		objCheckAppointmentDTO=objAppointmentDAO.getContactDetails(salesPersonName);
		
		return SUCCESS;
	}
	
}
