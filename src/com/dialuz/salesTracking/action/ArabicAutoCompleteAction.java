package com.dialuz.salesTracking.action;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;


import com.dialuz.salesTracking.DAO.ArabicAutocompleteDao;
import com.opensymphony.xwork2.ActionSupport;

public class ArabicAutoCompleteAction extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6694448294011164653L;
	private String name;
	
	private ArrayList<String> list=new ArrayList<String>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String execute()
	{
	HttpServletRequest request=ServletActionContext.getRequest();
	
		
		name=request.getParameter("q");
		
		ArabicAutocompleteDao objAutocomplteDao=new ArabicAutocompleteDao();
		list=objAutocomplteDao.getAutoComplte(name);
		return "success";
	}

	public ArrayList<String> getList() {
		return list;
	}

	public void setList(ArrayList<String> list) {
		this.list = list;
	}

}
