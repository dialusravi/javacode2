package com.dialuz.salesTracking.action;

import com.dialuz.salesTracking.DAO.UsersOperationsDAO;
import com.dialuz.salesTracking.DTO.UsersOperationsDTO;
import com.opensymphony.xwork2.ActionSupport;

public class SaveUpdateUserDetailsAction extends ActionSupport
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2547306342141260267L;
	/**
	 * 
	 */
	private UsersOperationsDTO objUsersOperationsDTO=new UsersOperationsDTO();
	private String success_msg;
	public UsersOperationsDTO getObjUsersOperationsDTO() {
		return objUsersOperationsDTO;
	}
	public void setObjUsersOperationsDTO(UsersOperationsDTO objUsersOperationsDTO) {
		this.objUsersOperationsDTO = objUsersOperationsDTO;
	}
	public String getSuccess_msg() {
		return success_msg;
	}
	public void setSuccess_msg(String success_msg) {
		this.success_msg = success_msg;
	}
	private String editoperation;
	public String execute()
	{
		boolean update_status=false;
		UsersOperationsDAO objUsersOperationsDAO=new UsersOperationsDAO();
		
		if(objUsersOperationsDTO.getUsertype().equals("telecaller"))
		{
			editoperation="telecaller";
			update_status=objUsersOperationsDAO.updateEditTelecallerDetails(objUsersOperationsDTO);
			
		}else if(objUsersOperationsDTO.getUsertype().equals("salesperson"))
		{
			editoperation="salesperson";
			update_status=objUsersOperationsDAO.updateEditSalesPersonDetails(objUsersOperationsDTO);
		}else {
			editoperation="teamleader";
			
			update_status=objUsersOperationsDAO.updateEditTeamLeaderDetails(objUsersOperationsDTO);
		}

		
		if(update_status)
		{
			
			success_msg="User Details Has Been Updated Successfully.............";
			return SUCCESS;
		}else
		{
			success_msg="Sorry Error Occured While Updating User Details.............";
			return SUCCESS;
		}
	}
	public String getEditoperation() {
		return editoperation;
	}
	public void setEditoperation(String editoperation) {
		this.editoperation = editoperation;
	}
}
