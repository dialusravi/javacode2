package com.dialuz.salesTracking.action;



import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.CityAutoCompleteSearchDAO;
import com.opensymphony.xwork2.ActionSupport;

public class AllocatedautoCompleteAction extends ActionSupport{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8750367735448186871L;
	private String q;
	private String variable;
	





	public String getVariable() {
		return variable;
	}

	public void setVariable(String variable) {
		this.variable = variable;
	}

	private ArrayList<String> NameList;


	

	public ArrayList<String> getNameList() {
		return NameList;
	}

	public void setNameList(ArrayList<String> nameList) {
		NameList = nameList;
	}

	
	public String getQ() {
		return q;
	}

	public void setQ(String q) {
		this.q = q;
	}
	
	
	public String execute() 
	{
		

		CityAutoCompleteSearchDAO objClassifiesHeaderSearchDAO=new CityAutoCompleteSearchDAO();
		
		try{
				
					NameList=objClassifiesHeaderSearchDAO.getautoCompleteNames(q);
				
		}
		catch (Exception e) 
		{
			
		}
		
		return SUCCESS;
	}
	
}
