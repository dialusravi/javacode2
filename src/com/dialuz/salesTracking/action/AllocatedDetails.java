package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.AppointmentDAO;
import com.dialuz.salesTracking.DTO.AppointmentDTO;
import com.opensymphony.xwork2.ActionSupport;

public class AllocatedDetails extends ActionSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1995493226169760799L;
	
private String name;
private String date;
private AppointmentDTO objCheckAppointmentDTO=new AppointmentDTO();
private ArrayList<AppointmentDTO> objNamesList=new ArrayList<AppointmentDTO>();
private ArrayList<AppointmentDTO> objNamesList1=new ArrayList<AppointmentDTO>();
public ArrayList<AppointmentDTO> getObjNamesList() {
	return objNamesList;
}
public void setObjNamesList(ArrayList<AppointmentDTO> objNamesList) {
	this.objNamesList = objNamesList;
}
public AppointmentDTO getObjCheckAppointmentDTO() {
	return objCheckAppointmentDTO;
}
public void setObjCheckAppointmentDTO(AppointmentDTO objCheckAppointmentDTO) {
	this.objCheckAppointmentDTO = objCheckAppointmentDTO;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getDate() {
	return date;
}
public void setDate(String date) {
	this.date = date;
}

public String execute(){
	
	AppointmentDAO objAppointmentDAO=new AppointmentDAO();
	objCheckAppointmentDTO.setName(name);
	objCheckAppointmentDTO.setDate(date);
	
	objNamesList=objAppointmentDAO.getAllocatedNames(objCheckAppointmentDTO);
	objNamesList1=objAppointmentDAO.getArabicAllocatedNames(objCheckAppointmentDTO);
	return "success";
	
}
public ArrayList<AppointmentDTO> getObjNamesList1() {
	return objNamesList1;
}
public void setObjNamesList1(ArrayList<AppointmentDTO> objNamesList1) {
	this.objNamesList1 = objNamesList1;
}
}
