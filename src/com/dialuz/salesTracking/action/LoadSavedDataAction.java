package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.dialuz.salesTracking.DAO.AppointmentDAO;
import com.dialuz.salesTracking.DTO.AppointmentDTO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class LoadSavedDataAction extends ActionSupport{

	
	private static final long serialVersionUID = 5679932387212436465L;
	private ArrayList<AppointmentDTO> objAppointmentDTOsList=new ArrayList<AppointmentDTO>();
	public ArrayList<AppointmentDTO> getObjAppointmentDTOsList() {
		return objAppointmentDTOsList;
	}
	public void setObjAppointmentDTOsList(ArrayList<AppointmentDTO> objAppointmentDTOsList) {
		this.objAppointmentDTOsList = objAppointmentDTOsList;
	}
	private String savedtodaydate;
	private String todaydate;
	
	public String execute()
	{
		
		HttpServletRequest request  = (HttpServletRequest)ActionContext.getContext().get(org.apache.struts2.StrutsStatics.HTTP_REQUEST);
		HttpSession session=request.getSession(true);
		
		AppointmentDAO objAppointmentDAO=new AppointmentDAO();
		AppointmentDTO objAppointmentDTO =new AppointmentDTO();
		objAppointmentDTO.setTodaydate(todaydate);
		objAppointmentDTO.setTelecallerName((String)session.getAttribute("username"));
		objAppointmentDTOsList=objAppointmentDAO.getSavedData(objAppointmentDTO);
		
		return SUCCESS;
	}
	public String getSavedtodaydate() {
		return savedtodaydate;
	}
	public void setSavedtodaydate(String savedtodaydate) {
		this.savedtodaydate = savedtodaydate;
	}
	public String getTodaydate() {
		return todaydate;
	}
	public void setTodaydate(String todaydate) {
		this.todaydate = todaydate;
	}

}
