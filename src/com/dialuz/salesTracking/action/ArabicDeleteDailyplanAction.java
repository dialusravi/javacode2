package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.BusinessDetailDao;
import com.dialuz.salesTracking.DTO.BusinessDetailDTO;
import com.opensymphony.xwork2.ActionSupport;

public class ArabicDeleteDailyplanAction extends ActionSupport {

	
	private static final long serialVersionUID = 4819558958810262952L;

	private int  business_id;
	private String city;
	  
	  ArrayList<BusinessDetailDTO> objArrayList=new ArrayList<BusinessDetailDTO>();

		public ArrayList<BusinessDetailDTO> getObjArrayList()
		{
			return objArrayList;
		}

		public void setObjArrayList(ArrayList<BusinessDetailDTO> objArrayList) 
		{
			this.objArrayList = objArrayList;
		}
	

	

	public String execute()
	{
		
						
		BusinessDetailDao objBusinessDetailDao=new BusinessDetailDao();
		int status=objBusinessDetailDao.deleteArabicDailyPlan(business_id);
					
		if(status>0)
		{			
			
		objArrayList =objBusinessDetailDao.getArabicDailyCallplan(city);
					
		return "success";
		
		}
	else{
			return "error";	
		}
		
	}

	public int getBusiness_id() {
		return business_id;
	}

	public void setBusiness_id(int business_id) {
		this.business_id = business_id;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}


	

}
