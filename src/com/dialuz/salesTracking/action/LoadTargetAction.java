package com.dialuz.salesTracking.action;

import javax.servlet.http.HttpServletRequest;


import com.dialuz.salesTracking.DAO.TargetDao;
import com.dialuz.salesTracking.DTO.TargetDTO;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class LoadTargetAction extends ActionSupport
{
	private String username;
	private TargetDTO objTargetDTO=new TargetDTO();
	private TargetDTO objTargetDTO1=new TargetDTO();
	 
	private Double todayachived1;
	private Double weekachived1;
	private Double monthachived1;

	public TargetDTO getObjTargetDTO() {
		return objTargetDTO;
	}

	public void setObjTargetDTO(TargetDTO objTargetDTO) {
		this.objTargetDTO = objTargetDTO;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String execute()
	{
		
		TargetDao objTargetDao=new TargetDao();
		objTargetDTO=objTargetDao.GetUserCount(username);
		objTargetDTO1=objTargetDao.GetTargets(username);
		
		//bar chart code begin
		javax.servlet.http.HttpServletRequest request  =(HttpServletRequest)ActionContext.getContext().get(org.apache.struts2.StrutsStatics.HTTP_REQUEST);
		
		int	todaycount1=objTargetDTO.getDay_count();
		
		int	weekcount1=objTargetDTO.getWeek_count();
		int	monthcount1=objTargetDTO.getMonth_count();
			
		int	todaytarget1=objTargetDTO1.getToday_target();
		int	weektarget1=objTargetDTO1.getWeek_target();
		int	monthtarget1=objTargetDTO1.getMonth_target();
			try{
				
				
		   todayachived1=(double)((todaycount1*100)/todaytarget1);
		   
			weekachived1=(double)((weekcount1*100)/weektarget1);
			monthachived1=(double)((monthcount1*100)/monthtarget1);			
			request.setAttribute("todayachived1",todayachived1 );
			request.setAttribute("weekachived1",weekachived1 );
			request.setAttribute("monthachived1",monthachived1);

			}catch (Exception e)
			{
				
			}
		
		//bar chart code end
		
		
		
		
		
		return "success";
	}

	public TargetDTO getObjTargetDTO1() {
		return objTargetDTO1;
	}

	public void setObjTargetDTO1(TargetDTO objTargetDTO1) {
		this.objTargetDTO1 = objTargetDTO1;
	}

	public Double getTodayachived1() {
		return todayachived1;
	}

	public void setTodayachived1(Double todayachived1) {
		this.todayachived1 = todayachived1;
	}

	public Double getWeekachived1() {
		return weekachived1;
	}

	public void setWeekachived1(Double weekachived1) {
		this.weekachived1 = weekachived1;
	}

	public Double getMonthachived1() {
		return monthachived1;
	}

	public void setMonthachived1(Double monthachived1) {
		this.monthachived1 = monthachived1;
	}
}
