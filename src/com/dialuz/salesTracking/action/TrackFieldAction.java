package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.BusinessDetailDao;
import com.dialuz.salesTracking.DTO.TrackFieldDTO;
import com.opensymphony.xwork2.ActionSupport;

public class TrackFieldAction extends ActionSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3491380003377567150L;
	
	private ArrayList<TrackFieldDTO> objArrayList=new ArrayList<TrackFieldDTO>();

	
	public ArrayList<TrackFieldDTO> getObjArrayList() {
		return objArrayList;
	}


	public void setObjArrayList(ArrayList<TrackFieldDTO> objArrayList) {
		this.objArrayList = objArrayList;
	}


	public String execute(){
	BusinessDetailDao objBusinessDetailDao=new BusinessDetailDao();
	objArrayList=objBusinessDetailDao.getTrackFieldSales();
	return "success";
}
}