package com.dialuz.salesTracking.action;

import com.dialuz.salesTracking.DAO.UsersOperationsDAO;
import com.dialuz.salesTracking.DTO.UsersOperationsDTO;
import com.opensymphony.xwork2.ActionSupport;

public class DeleteUsersAction extends ActionSupport
{

	/**
	 * 
	 */
	/**
	 * 
	 */
	private static final long serialVersionUID = 5607284747835630420L;
	private String type;
	private String userId;
	private String success_msg;
	private String operation;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getSuccess_msg() {
		return success_msg;
	}
	public void setSuccess_msg(String success_msg) {
		this.success_msg = success_msg;
	}
	
	public String execute()
	{
		
		boolean update_status=false;
		UsersOperationsDAO objUsersOperationsDAO=new UsersOperationsDAO();
		UsersOperationsDTO objUsersOperationsDTO=new UsersOperationsDTO();
		objUsersOperationsDTO.setUserId(userId);
		if(type.equals("telecaller"))
		{
			operation="deletetelecaller";
			update_status=objUsersOperationsDAO.deleteTelecallers(objUsersOperationsDTO);
		}else if(type.equals("teamleader"))
		{
			
			operation="deleteteamleader";
			update_status=objUsersOperationsDAO.deleteTeamLeader(objUsersOperationsDTO);
		}else {
			operation="deletesalesperson";
			update_status=objUsersOperationsDAO.deleteSalesPersons(objUsersOperationsDTO);
		}
		
		if(update_status)
		{
			success_msg="User Deleted Successfully...............";
			return SUCCESS;
		}else
		{
			success_msg="Error Occured During  Deleting Record ...............";
			return SUCCESS;
		}
			
		
		
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
}
