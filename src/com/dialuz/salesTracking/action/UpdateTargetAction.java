package com.dialuz.salesTracking.action;


import com.dialuz.salesTracking.DAO.TargetDao;
import com.opensymphony.xwork2.ActionSupport;

public class UpdateTargetAction extends ActionSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5744567243430059977L;
	
	private String changetelename;
	private String changetargets;
	private String target_success;
	
	public String execute()
	{
		TargetDao objTargetDao=new TargetDao();
		int status=objTargetDao.updateTarget(changetelename,changetargets);
		if(status==1)
		setTarget_success("Targets SuccessFully Updated");	
		else
		setTarget_success("SuccessFully Not Updated");	
		
		
		
		
		
		
		
		return "success";
	}
	public String getChangetelename() {
		return changetelename;
	}
	public void setChangetelename(String changetelename) {
		this.changetelename = changetelename;
	}
	public String getChangetargets() {
		return changetargets;
	}
	public void setChangetargets(String changetargets) {
		this.changetargets = changetargets;
	}
	public String getTarget_success() {
		return target_success;
	}
	public void setTarget_success(String target_success) {
		this.target_success = target_success;
	}
	

}
