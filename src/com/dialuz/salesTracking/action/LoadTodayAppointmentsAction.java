package com.dialuz.salesTracking.action;

import java.util.ArrayList;

import com.dialuz.salesTracking.DAO.AppointmentDAO;
import com.dialuz.salesTracking.DTO.AppointmentDTO;
import com.opensymphony.xwork2.ActionSupport;

public class LoadTodayAppointmentsAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<AppointmentDTO> objTodayAppointmentlist=new ArrayList<AppointmentDTO>();
	private ArrayList<AppointmentDTO> objTodayAppointmentlist1=new ArrayList<AppointmentDTO>();
	public String execute()
	{
		AppointmentDAO objTodayAppointmentDAO=new AppointmentDAO();
	
		objTodayAppointmentlist=objTodayAppointmentDAO.getTodayAppointmentDetails();
		objTodayAppointmentlist1=objTodayAppointmentDAO.getTodayAppointmentDetails1();
		
	
		
		
		return SUCCESS;
	}
	public ArrayList<AppointmentDTO> getObjTodayAppointmentlist() {
		return objTodayAppointmentlist;
	}
	public void setObjTodayAppointmentlist(
			ArrayList<AppointmentDTO> objTodayAppointmentlist) {
		this.objTodayAppointmentlist = objTodayAppointmentlist;
	}
	public ArrayList<AppointmentDTO> getObjTodayAppointmentlist1() {
		return objTodayAppointmentlist1;
	}
	public void setObjTodayAppointmentlist1(
			ArrayList<AppointmentDTO> objTodayAppointmentlist1) {
		this.objTodayAppointmentlist1 = objTodayAppointmentlist1;
	}
	
	
	
}
