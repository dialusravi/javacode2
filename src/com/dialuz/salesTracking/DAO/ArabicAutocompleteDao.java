package com.dialuz.salesTracking.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.dialuz.salesTracking.DB.DBConfig;



public class ArabicAutocompleteDao {

	public ArrayList<String> getAutoComplte(String name)
	{

		  ArrayList<String> list=new ArrayList<String>();
			Connection objConnection = null;
			PreparedStatement objPreparedStatement = null;
			ResultSet objResultSet = null;

		 try {				 				 
					 objConnection = DBConfig.connectArabicDb();
					 String query="SELECT distinct keyword FROM keywords  where  keyword like '"+name+"%' LIMIT 0,5";
					
					 objPreparedStatement = objConnection.prepareStatement(query);
					
					 objResultSet = objPreparedStatement.executeQuery();
					
					while(objResultSet.next())
					 {
						
						list.add(objResultSet.getString("keyword"));
			    			
					 }
					
			 } 
			 catch (Exception e) {
				e.printStackTrace();
			 } 
			 finally {
				
				 DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);

			 }

			 return list;
		
		
		
		
	}
	public ArrayList getTargetUserType(String targetuser) {
		Connection objcon=null;
		PreparedStatement objpstm=null;
		ResultSet objrs=null;
		ArrayList 	objArralist=new ArrayList();
		String sqlqueary="SELECT username FROM user_details where username like '"+targetuser+"%'";
		
		try
		{
			objcon=DBConfig.connectToMainDB();
			objpstm = objcon.prepareStatement(sqlqueary);
			objrs= objpstm.executeQuery();
			
			while (objrs.next())
			{		
				
				objArralist.add(objrs.getString("username"));
				
			}
			
			
		}
		catch(Exception e)
		{
			
			e.printStackTrace();
		}
		finally
		{
			try
			{  
				
				
				DBConfig.disconnect(objcon,objpstm,objrs);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				
			}
		}
		return objArralist;
	}
	public ArrayList<String> getAllDetails()
	{
		  ArrayList<String> list=new ArrayList<String>();
			Connection objConnection = null;
			PreparedStatement objPreparedStatement = null;
			ResultSet objResultSet = null;

		 try {
				 				 
					 objConnection = DBConfig.connectArabicDb();
					
					 String query="SELECT distinct keyword FROM keywords";
					
					 objPreparedStatement = objConnection.prepareStatement(query);
					
					 objResultSet = objPreparedStatement.executeQuery();
					
					while(objResultSet.next())
					 {
						
					list.add(objResultSet.getString("keyword"));
			    			
					 }
					
			 } 
			 catch (Exception e) {
				e.printStackTrace();
			 } 
			 finally {
				
				 DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);

			 }

			 return list;
	}

	public String insertCountry(String country,String city)
	{
		 Connection objConnection = null;
	     PreparedStatement objPreparedStatement = null;
	     int result=0;
	     try {
				 
			 objConnection = DBConfig.connectArabicDb();
			 String query="insert into keywords(keyword,business_city) values(?,?)";
			
			 objPreparedStatement = objConnection.prepareStatement(query);
			 objPreparedStatement.setString(1, country);
			 objPreparedStatement.setString(2, city);
			 result=objPreparedStatement.executeUpdate();
			
			
			
	 } 
	 catch (Exception e) 
	 {
		e.printStackTrace();
	 } 
	     
	 finally
     {
		
		 DBConfig.disconnect(objConnection, objPreparedStatement, null);

	 }if(result>0)
     {
    	 return "true";
     }

	     return "false";
	}

	public int arabicDeleteKeyword(String delstr, String city) {
		 Connection objConnection = null;
	     PreparedStatement objPreparedStatement = null;
	     int result=0;
	     try {
				 
			 objConnection = DBConfig.connectArabicDb();
			 String query="delete from keywords where keyword=? and business_city=?";
			
			 objPreparedStatement = objConnection.prepareStatement(query);
			 objPreparedStatement.setString(1, delstr);
			 objPreparedStatement.setString(2, city);
			 result=objPreparedStatement.executeUpdate();
			
			
			
	 } 
	 catch (Exception e) 
	 {
		e.printStackTrace();
	 } 
	     
	 finally
     {
		
		 DBConfig.disconnect(objConnection, objPreparedStatement, null);

	 }if(result>0)
     {
    	 return 1;
     }

	     return 0;
	}

	

}
