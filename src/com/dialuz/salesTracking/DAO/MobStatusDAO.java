package com.dialuz.salesTracking.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.dialuz.salesTracking.DB.DBConfig;

public class MobStatusDAO
{
	
	public boolean checkbusinessMob(String businessMob1,String type) 
	{
		boolean update_status=false;
		Connection objconnection=DBConfig.connect();
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		//id, user_name, password, user_type
		String sql="";
		if(type.equals("sales"))
		{
			sql="select id from sales_user_details_arabic  where status_flag=0  and   mobile_no='"+businessMob1+"'";
		}else
		{
			sql="select telecaller_id  from  telecaller_details_arabic  where  status_flag=0  and  telecaller_mobileno='"+businessMob1+"'";
		}
		try{
			objPreparedStatement=objconnection.prepareStatement(sql);
			objResultSet=objPreparedStatement.executeQuery();
			if(objResultSet.next())
			{
					update_status=true;
				
			}else
			{
				update_status=false;
			}
			
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally{
			DBConfig.disconnect(objconnection, objPreparedStatement, objResultSet);
		}
		
		return update_status;
	
	}
	

}
