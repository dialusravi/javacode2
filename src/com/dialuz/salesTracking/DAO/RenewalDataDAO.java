package com.dialuz.salesTracking.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;



import java.util.ArrayList;

import com.dialuz.salesTracking.DB.DBConfig;
import com.dialuz.salesTracking.DTO.RenewalDataDTO;
import com.mysql.jdbc.Statement;

public class RenewalDataDAO {

	public ArrayList<RenewalDataDTO> getRenewalData()
	{
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		String sql="";
		
		ArrayList<RenewalDataDTO> objArrayList=new ArrayList<RenewalDataDTO>();
		try{
			objConnection=DBConfig.connect();
			
				sql="SELECT  * FROM appointment_details_english  WHERE callback_date BETWEEN CURDATE() AND DATE_add(CURDATE(),INTERVAL 1 MONTH) limit 0,20 ";
				objPreparedStatement=objConnection.prepareStatement(sql);
				
				
				objResultSet=objPreparedStatement.executeQuery();
			
				while(objResultSet.next())
			{
					RenewalDataDTO objRenewalDataDTO=new RenewalDataDTO();
					
				objRenewalDataDTO.setBusiness_name(objResultSet.getString("business_name"));
				objRenewalDataDTO.setCallback_date(objResultSet.getString("callback_date"));
				objRenewalDataDTO.setContact_person(objResultSet.getString("contact_person"));
				objRenewalDataDTO.setComments(objResultSet.getString("comments"));
				objRenewalDataDTO.setBusiness_mobile(objResultSet.getString("business_mobile"));
				
				objArrayList.add(objRenewalDataDTO);
			}
				
		}catch(Exception e){e.printStackTrace();}
		finally{
			DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);
		}
		
		return objArrayList;
	}

	public ArrayList<RenewalDataDTO> getArabicRenewalData() {
		Connection objConnection=null;
		PreparedStatement objPreparedStatement=null;
		ResultSet objResultSet=null;
		String sql="";
		
		ArrayList<RenewalDataDTO> objArrayList=new ArrayList<RenewalDataDTO>();
		try{
			objConnection=DBConfig.connect();
			
				sql="SELECT  * FROM appointment_details_arabic  WHERE callback_date BETWEEN CURDATE() AND DATE_add(CURDATE(),INTERVAL 1 MONTH) limit 0,20 ";
				objPreparedStatement=objConnection.prepareStatement(sql);
				
				
				objResultSet=objPreparedStatement.executeQuery();
			
				while(objResultSet.next())
			{
					RenewalDataDTO objRenewalDataDTO=new RenewalDataDTO();
					
				objRenewalDataDTO.setBusiness_name(objResultSet.getString("business_name"));
				objRenewalDataDTO.setCallback_date(objResultSet.getString("callback_date"));
				objRenewalDataDTO.setContact_person(objResultSet.getString("contact_person"));
				objRenewalDataDTO.setComments(objResultSet.getString("comments"));
				objRenewalDataDTO.setBusiness_mobile(objResultSet.getString("business_mobile"));
				
				objArrayList.add(objRenewalDataDTO);
			}
				
		}catch(Exception e){e.printStackTrace();}
		finally{
			DBConfig.disconnect(objConnection, objPreparedStatement, objResultSet);
		}
		
		return objArrayList;
	}
	
	
	
	
	
	
	
	
}
