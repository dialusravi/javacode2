package com.dialuz.salesTracking.DB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class DBConfig{
	
	
	
	public static Connection connect(){
	   
		Connection objConnection = null;
		Context  objContext =null;
		DataSource objDataSource = null;
	
		try 
		{
			
			objContext =(Context) (new InitialContext()).lookup("java:comp/env");
			objDataSource = (DataSource)objContext.lookup("jdbc/salesTrackingDBArabic");
	
			
		} 
		catch (NamingException e) {
			e.printStackTrace();
			
			
		}
		
		try
		{
			objConnection = objDataSource.getConnection();
		}
		catch(SQLException e){
			
	
			e.printStackTrace();
		}
		
	 return objConnection;
	 }
	public static Connection connectArabicDb(){
		   
		Connection objConnection = null;
		Context  objContext =null;
		DataSource objDataSource = null;
	
		try 
		{
			
			objContext =(Context) (new InitialContext()).lookup("java:comp/env");
			objDataSource = (DataSource)objContext.lookup("jdbc/arabicdb");
	
			
		} 
		catch (NamingException e) {
			e.printStackTrace();
			
			
		}
		
		try
		{
			objConnection = objDataSource.getConnection();
		}
		catch(SQLException e){
			
	
			e.printStackTrace();
		}
		
	 return objConnection;
	 }

	public static Connection connectToMainDB(){
	   
		Connection objConnection = null;
		Context  objContext =null;
		DataSource objDataSource = null;
	
		try 
		{
			
			objContext =(Context) (new InitialContext()).lookup("java:comp/env");
			objDataSource = (DataSource)objContext.lookup("jdbc/indiadb");
	
			
		} 
		catch (NamingException e) 
		{
			e.printStackTrace();
						
		}
		
		try
		{
			objConnection = objDataSource.getConnection();
		}
		catch(SQLException e)
		{
		   e.printStackTrace();
		}
		
	      return objConnection;
	 }
	
	
	public static  void disconnect(Connection objConnection,PreparedStatement objPreparedStatement, ResultSet objResultSet)
	{
		 
		try{
             
			if(objResultSet!=null)
			{ 
             
				objResultSet.close();
             }
        }
		catch(Exception e)
		 {
             
        	e.printStackTrace();
             
         }

	    try{
             
	    	if(objPreparedStatement!=null)
	    	{
             
	    		objPreparedStatement.close();
             }
          }
	    catch(Exception e)
	    {
            
        	 e.printStackTrace();
         }   
	    try{
       
	    	if(objConnection!=null){
             
	    		objConnection.close();
	    	}
            
	    }
	    catch(Exception e)
	     {
            
	    	e.printStackTrace();
	    	
         }
	}
}

